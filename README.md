# Comunicación

Materiales usados por el equipo de comunicación de Rlyeh

## Versiones

El hacklab a lo largo de su historia pasó por varias identidades visuales.

Pedimos que quienes ocupen el hacklab se encarguen de mantener esta documentación
actualizada.

Las identidades fueron y son:
- Versión antigua: con pulpito "ovni" y pulpito "forzudo"
- Versión calavera (pre-rebranding): con pulpito "calavera" y rlyeh park
- Versión rebranding: con "triángulo-tentáculo" y arte ascii
